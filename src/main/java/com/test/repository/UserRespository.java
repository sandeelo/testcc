package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.User;


public interface UserRespository extends JpaRepository<User, Integer> {
	
	User findByEmail(String email);
	User findOneById(Integer id);
}
