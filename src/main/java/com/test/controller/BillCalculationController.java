package com.test.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.common.AppMessages;
import com.test.common.exception.AppException;
import com.test.common.utils.ExceptionUtil;
import com.test.entity.User;
import com.test.model.CreateBillPostModel;
import com.test.service.BillCalculationService;

@RestController
@RequestMapping(path="/api") 
public class BillCalculationController {

	@Autowired
	private ExceptionUtil exceptionUtil;

	@Autowired
	private BillCalculationService billCalculationService;

	@GetMapping("/user")
	ResponseEntity getAllUsers() {
		try {
			List<User> users = billCalculationService.getAllUsers();
			return new ResponseEntity<>(users, HttpStatus.OK);
		}
		catch (Exception exception) {
			return new ResponseEntity<>(AppMessages.GENERAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/user/{userId}/bill")
	ResponseEntity creatUserBill(@PathVariable(name = "userId") Integer userId,@Valid @RequestBody CreateBillPostModel createBillPostModel, BindingResult result) {
		try {
			System.out.print("request received");

			if (result.hasErrors()) {
				return new ResponseEntity<>(result.getAllErrors(), HttpStatus.BAD_REQUEST);
			}

			float billAmount = billCalculationService.getBillAmount(userId,createBillPostModel);
			Map<String, Float> billDetails = new HashMap<String, Float>();
			billDetails.put("Amount", billAmount);
			return new ResponseEntity<>(billDetails, HttpStatus.CREATED);
		}
		catch (AppException appException) {
			appException.printStackTrace();
			return new ResponseEntity<>(exceptionUtil.mapToClass(appException),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch (Exception exception) {
			exception.printStackTrace();
			return new ResponseEntity<>(AppMessages.GENERAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
