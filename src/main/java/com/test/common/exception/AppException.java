package com.test.common.exception;

import org.springframework.http.HttpStatus;

public class AppException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 602210140766824264L;
	private HttpStatus errorCode;
	
	public HttpStatus getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(HttpStatus errorCode) {
		this.errorCode = errorCode;
	}

	public AppException(String message) {
		super(message);
	}
	
	
}
