package com.test.common.utils;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.test.common.exception.AppException;
import com.test.common.model.ExceptionClass;

@Component
public class ExceptionUtil {

	public AppException generateAppException(HttpStatus httpStatus, String message) {
		AppException appException = new AppException(message);
		appException.setErrorCode(httpStatus);
		return appException;
	}
	
	public ExceptionClass mapToClass(AppException appException) {
		
		ExceptionClass exceptionClass = new ExceptionClass();
		exceptionClass.setErrorCode(appException.getErrorCode().toString());
		exceptionClass.setMessage(appException.getMessage());

		return exceptionClass;
	}
}
