package com.test.common;

public class AppMessages {
	
	public static final String GENERAL_ERROR = "This is for general error";
	public static final String INVALID_USER_ID = "UserId is invalid";
}
