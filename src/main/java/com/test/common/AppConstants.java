package com.test.common;

public class AppConstants {

	public static final float AFFILIATE_DISCOUNT = 10;
	public static final float OLD_CUSTOMER_DISCOUNT = 5;
	public static final float EMPLOYEE_DISCOUNT = 30;
	public static final float OLD_CUSTOMER_YEAR_THRESHOLD = 5;
	
}
