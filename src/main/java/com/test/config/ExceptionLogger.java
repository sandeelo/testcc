package com.test.config;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import com.test.common.exception.AppException;

@Configuration
@Aspect
public class ExceptionLogger{
    @AfterThrowing(pointcut = "execution(* com.test.config.*.*.*(..))", throwing = "ex")
    public void logError(AppException ex) {
    	System.out.print("Will be adding centralized logging here -------------->");
        ex.printStackTrace();
    }
}