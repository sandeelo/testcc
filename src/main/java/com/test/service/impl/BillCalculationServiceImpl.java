package com.test.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.common.AppConstants;
import com.test.common.AppMessages;
import com.test.common.exception.AppException;
import com.test.common.utils.DateUtil;
import com.test.common.utils.ExceptionUtil;
import com.test.entity.User;
import com.test.model.CreateBillPostModel;
import com.test.model.Item;
import com.test.repository.UserRespository;
import com.test.service.BillCalculationService;


@Service
public class BillCalculationServiceImpl implements BillCalculationService {

	@Autowired
	private UserRespository userRespository;

	@Autowired
	private  ExceptionUtil exceptionUtil;

	@Autowired
	private  DateUtil dateUtil;

	@Override
	public float getBillAmount(Integer userId, CreateBillPostModel dataModel) throws AppException {

		try{
			User user =  getUserById(userId);
			if(user == null) {
				throw exceptionUtil.generateAppException(HttpStatus.BAD_REQUEST,AppMessages.INVALID_USER_ID);
			}

			float totalBill = 0 ;
			float groceryAmount = 0 ;
			float otherAmount= 0 ;
			float otherAmountDiscount= 0 ;

			for(Item item : dataModel.getItemList()) {

				if(item.getType()==Item.ItemType.NON_GROCERY) {
					otherAmount+=item.getCost();
				}
				else if(item.getType()==Item.ItemType.GROCERY) {
					groceryAmount+=item.getCost();
				}
			}

			if(user.isEmployee()) {
				otherAmountDiscount = (otherAmount/100) * AppConstants.EMPLOYEE_DISCOUNT;
			}
			else if(user.isAffiliate()) {
				otherAmountDiscount = (otherAmount/100) * AppConstants.AFFILIATE_DISCOUNT;
			}
			else if(dateUtil.getDiffYears(user.getCreatedAt(), Calendar.getInstance().getTime())>=AppConstants.OLD_CUSTOMER_YEAR_THRESHOLD) {
				otherAmountDiscount = (otherAmount/100) * AppConstants.OLD_CUSTOMER_DISCOUNT;
			}
			totalBill = groceryAmount + otherAmount-otherAmountDiscount;
			int discount =(int) totalBill/100;
			discount = discount * 5;
			totalBill = totalBill - discount;
			return totalBill;
		}
		catch (AppException e) {
			e.printStackTrace();
			throw exceptionUtil.generateAppException(e.getErrorCode(),e.getMessage());
		}
	}

	@Override
	public List<User> getAllUsers() {
		return userRespository.findAll();
	}

	@Override
	public User getUserById(Integer id) {
		return userRespository.findOneById(id);
	}
}
