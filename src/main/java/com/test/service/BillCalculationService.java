package com.test.service;

import java.util.List;

import com.test.common.exception.AppException;
import com.test.entity.User;
import com.test.model.CreateBillPostModel;


public interface BillCalculationService {


	float getBillAmount(Integer userId, CreateBillPostModel createBillPostModel) throws AppException;
	List<User> getAllUsers();
	User getUserById(Integer userId);
}
