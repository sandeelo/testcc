package com.test.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"itemList"
})
public class CreateBillPostModel {

@JsonProperty("itemList")
private List<Item> itemList = null;

@JsonProperty("itemList")
public List<Item> getItemList() {
return itemList;
}

@JsonProperty("itemList")
public void setItemList(List<Item> itemList) {
this.itemList = itemList;
}


}
