package com.test.model;



import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"name",
"cost",
"type"
})
public class Item {


	public enum ItemType {

			GROCERY,
			NON_GROCERY
	}
	 

	@NotNull(message = "Name is mandatory")
	private String name;
	@NotNull(message = "Cost is mandatory")
	private Integer cost;
	@NotNull(message = "Type is mandatory")
	private ItemType type;

	public String getName() {
	return name;
	}
	
	public void setName(String name) {
	this.name = name;
	}

	public Integer getCost() {
	return cost;
	}
	
	public void setCost(Integer cost) {
	this.cost = cost;
	}
	
	public ItemType getType() {
	return type;
	}
	
	public void setType(ItemType type) {
	this.type = type;
	}
	

}