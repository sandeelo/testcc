package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestAppApplicationTests {


	
	private static final String URL_POST_BILL_CALCULATION = "http://localhost:8080/api/user/1/bill";
    
    @Test
    public void Test_MixedItems() {

    	try {
    		
	    	HttpHeaders postHeaders = new HttpHeaders();
	        postHeaders.setContentType(MediaType.APPLICATION_JSON);
	
	        TestRestTemplate restTemplate = new TestRestTemplate();
	        
	        String requestJson = "{\n" + 
	        		"\n" + 
	        		"\"itemList\":[\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 1\",\n" + 
	        		"   \"cost\":90,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 2\",\n" + 
	        		"   \"cost\":85,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 3\",\n" + 
	        		"   \"cost\":80,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 4\",\n" + 
	        		"   \"cost\":190,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	}\n" + 
	        		"	\n" + 
	        		"]\n" + 
	        		"}";
	        HttpEntity<String> json = new HttpEntity<String>(requestJson, postHeaders);
	        ResponseEntity<String> result = restTemplate.exchange(URL_POST_BILL_CALCULATION, HttpMethod.POST, json, String.class);
	         
	     
        JSONAssert.assertEquals("{\n" + 
        		"    \"Amount\": 398.0\n" + 
        		"}", result.getBody(), false);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    }


    @Test
    public void Test_AllGrocery() {

    	try {
    		
	    	HttpHeaders postHeaders = new HttpHeaders();
	        postHeaders.setContentType(MediaType.APPLICATION_JSON);
	
	        TestRestTemplate restTemplate = new TestRestTemplate();
	        
	        String requestJson = "{\n" + 
	        		"\n" + 
	        		"\"itemList\":[\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 1\",\n" + 
	        		"   \"cost\":90,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 2\",\n" + 
	        		"   \"cost\":85,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 3\",\n" + 
	        		"   \"cost\":80,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 4\",\n" + 
	        		"   \"cost\":190,\n" + 
	        		"   \"type\":\"GROCERY\"\n" + 
	        		"	}\n" + 
	        		"	\n" + 
	        		"]\n" + 
	        		"}";
	        HttpEntity<String> json = new HttpEntity<String>(requestJson, postHeaders);
	        ResponseEntity<String> result = restTemplate.exchange(URL_POST_BILL_CALCULATION, HttpMethod.POST, json, String.class);
	         
	     
        JSONAssert.assertEquals("{\n" + 
        		"    \"Amount\": 425\n" + 
        		"}", result.getBody(), false);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    }

    
    @Test
    public void Test_AllNonGrocery() {

    	try {
    		
	    	HttpHeaders postHeaders = new HttpHeaders();
	        postHeaders.setContentType(MediaType.APPLICATION_JSON);
	
	        TestRestTemplate restTemplate = new TestRestTemplate();
	        
	        String requestJson = "{\n" + 
	        		"\n" + 
	        		"\"itemList\":[\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 1\",\n" + 
	        		"   \"cost\":90,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 2\",\n" + 
	        		"   \"cost\":85,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 3\",\n" + 
	        		"   \"cost\":80,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 4\",\n" + 
	        		"   \"cost\":190,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	}\n" + 
	        		"	\n" + 
	        		"]\n" + 
	        		"}";
	        HttpEntity<String> json = new HttpEntity<String>(requestJson, postHeaders);
	        ResponseEntity<String> result = restTemplate.exchange(URL_POST_BILL_CALCULATION, HttpMethod.POST, json, String.class);
	         
	     
        JSONAssert.assertEquals("{\n" + 
        		"    \"Amount\": 296.5\n" + 
        		"}", result.getBody(), false);
    	}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    }

    @Test
    public void Test_InvalidItemType() {

    	try {
    		
	    	HttpHeaders postHeaders = new HttpHeaders();
	        postHeaders.setContentType(MediaType.APPLICATION_JSON);
	
	        TestRestTemplate restTemplate = new TestRestTemplate();
	        
	        String requestJson = "{\n" + 
	        		"\n" + 
	        		"\"itemList\":[\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 1\",\n" + 
	        		"   \"cost\":90,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 2\",\n" + 
	        		"   \"cost\":85,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 3\",\n" + 
	        		"   \"cost\":80,\n" + 
	        		"   \"type\":\"NON_\"\n" + 
	        		"	},\n" + 
	        		"	{\n" + 
	        		"   \"name\":\"item 4\",\n" + 
	        		"   \"cost\":190,\n" + 
	        		"   \"type\":\"NON_GROCERY\"\n" + 
	        		"	}\n" + 
	        		"	\n" + 
	        		"]\n" + 
	        		"}";
	        HttpEntity<String> json = new HttpEntity<String>(requestJson, postHeaders);
	        ResponseEntity<String> result = restTemplate.exchange(URL_POST_BILL_CALCULATION, HttpMethod.POST, json, String.class);
	         
	        assertEquals(HttpStatus.BAD_REQUEST,  result.getStatusCode());
    	}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    }
    
	}