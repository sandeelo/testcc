How to deploy it on your machine:

1) Open the db.sql file in the project root directory and execute it
2) Update the mysql config in application.properties 
3) src/test/java has a class with name TestAppApplicationTests.java in com/test, you can execute it directly which covers some basic test cases.


Tools and Technology 

1)	Spring boot
2)	Spring data jpa (basic)
3)	Maven
4)	MySQL

 
Notes:
•	/api/user/{userId}/bill (POST)
   o	As per pure rest standard it should not return any reponse
   o	The response should be returned in location headers with a redirection to the id of the resource added
   o	Since it was a test so I didn’t store the itemList in another table and showed the response on the response of the POST call.
